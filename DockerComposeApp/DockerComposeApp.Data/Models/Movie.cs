using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DockerComposeApp.Data.Models
{
    public class Movie
    {
        [Key]
        public int MovieId { get; set; }

        public string Title { get; set; }

        public int Year { get; set; }

        public ICollection<MovieCastMember> MovieCastMembers { get; set; }

        public ICollection<MovieGenre> MovieGenres { get; set; }
    }
}
