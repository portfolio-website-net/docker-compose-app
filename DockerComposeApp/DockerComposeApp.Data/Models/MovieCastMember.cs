using System.ComponentModel.DataAnnotations;

namespace DockerComposeApp.Data.Models
{
    public class MovieCastMember
    {
        [Key]
        public int MovieCastMemberId { get; set; }

        public int MovieId { get; set; }

        public Movie Movie { get; set; }

        public int CastMemberId { get; set; }

        public CastMember CastMember { get; set; }
    }
}
