using System.ComponentModel.DataAnnotations;

namespace DockerComposeApp.Data.Models
{
    public class Genre
    {
        [Key]
        public int GenreId { get; set; }

        public string Name { get; set; }
    }
}
