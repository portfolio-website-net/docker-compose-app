using System.ComponentModel.DataAnnotations;

namespace DockerComposeApp.Data.Models
{
    public class CastMember
    {
        [Key]
        public int CastMemberId { get; set; }

        public string Name { get; set; }
    }
}
