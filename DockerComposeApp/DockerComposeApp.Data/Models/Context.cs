using Microsoft.EntityFrameworkCore;

namespace DockerComposeApp.Data.Models
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieCastMember>().HasKey(x => new { x.MovieId, x.CastMemberId });
            modelBuilder.Entity<MovieGenre>().HasKey(x => new { x.MovieId, x.GenreId });
        }

        public DbSet<Movie> Movies { get; set; }

        public DbSet<CastMember> CastMembers { get; set; }

        public DbSet<Genre> Genres { get; set; }

        public DbSet<MovieCastMember> MovieCastMembers { get; set; }

        public DbSet<MovieGenre> MovieGenres { get; set; }

        public DbSet<Answer> Answers { get; set; }
    }
}
