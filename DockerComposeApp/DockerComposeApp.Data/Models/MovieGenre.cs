using System.ComponentModel.DataAnnotations;

namespace DockerComposeApp.Data.Models
{
    public class MovieGenre
    {
        [Key]
        public int MovieGenreId { get; set; }

        public int MovieId { get; set; }

        public Movie Movie { get; set; }

        public int GenreId { get; set; }

        public Genre Genre { get; set; }
    }
}
