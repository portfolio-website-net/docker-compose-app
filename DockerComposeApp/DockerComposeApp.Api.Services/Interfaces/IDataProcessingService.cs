﻿using DockerComposeApp.Shared.Requests;
using DockerComposeApp.Shared.Responses;

namespace DockerComposeApp.Api.Services.Interfaces
{
    public interface IDataProcessingService
    {
        AnswerResponse GetAnswers(AnswerRequest answerRequest);
    }
}