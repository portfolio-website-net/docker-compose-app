﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using DockerComposeApp.Api.Services.Interfaces;
using DockerComposeApp.Data.Models;
using DockerComposeApp.Shared;
using DockerComposeApp.Shared.Enums;
using DockerComposeApp.Shared.Requests;
using DockerComposeApp.Shared.Responses;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace DockerComposeApp.Api.Services
{
    public class DataProcessingService : IDataProcessingService
    {
        static readonly object lockObj = new object();

        private readonly Context _context;
        private readonly Settings _settings;

        public DataProcessingService(Context context, IOptions<Settings> settings)
        {
            _context = context;
            _settings = settings.Value;
        }

        public AnswerResponse GetAnswers(AnswerRequest answerRequest)
        {
            var answerResponse = new AnswerResponse();
            foreach (var question in answerRequest.Question)
            {
                answerResponse.Answer.Add(question, GetAnswer(question));
            }

            PersistAnswersIfEmpty(answerResponse);

            return answerResponse;
        }

        private string GetAnswer(Question question)
        {
            var result = string.Empty;

            InitializeRecordsIfEmpty();

            switch(question)
            {
                case Question.CountSince1975:
                    var countSince1975 = _context.Movies.Where(x => x.Year >= 1975);
                    result = $"{string.Format("{0:n0}", countSince1975.Count())}";
                    break;

                case Question.MostCommonYear:
                    var mostCommonYear = _context.Movies.GroupBy(x => x.Year)
                                        .Select(group => new
                                        {
                                            Year = group.Key,
                                            Count = group.Count()
                                        })
                                        .OrderByDescending(x => x.Count)
                                        .First().Year;
                    result = $"{mostCommonYear}";
                    break;

                case Question.TotalCastMembers:
                    var totalCastMembers = _context.Movies.Sum(x => x.MovieCastMembers.Count());
                    result = $"{string.Format("{0:n0}", totalCastMembers)}";
                    break;
            }

            return result;
        }

        private void InitializeRecordsIfEmpty()
        {
            if (_context.Movies.Count() == 0)
            {
                // Only permit one thread at a time
                lock (lockObj)
                {
                    // If the Movies table is still empty, then load and insert the records
                    if (_context.Movies.Count() == 0)
                    {
                        var records = LoadRecords(_settings.DataFileName);
                        foreach (var record in records)
                        {
                            var movie = new Movie
                            {
                                Title = record.title,
                                Year = record.year
                            };

                            foreach (var name in record.cast)
                            {
                                var castMember = _context.CastMembers.FirstOrDefault(x => x.Name == name);

                                if (castMember == null)
                                {
                                    castMember = new CastMember
                                    {
                                        Name = name
                                    };

                                    _context.CastMembers.Add(castMember);
                                }

                                var movieCastMember = new MovieCastMember
                                {
                                    Movie = movie,
                                    CastMember = castMember
                                };

                                _context.MovieCastMembers.Add(movieCastMember);
                            }

                            foreach (var name in record.genres)
                            {
                                var genre = _context.Genres.FirstOrDefault(x => x.Name == name);

                                if (genre == null)
                                {
                                    genre = new Genre
                                    {
                                        Name = name
                                    };

                                    _context.Genres.Add(genre);
                                }

                                var movieGenre = new MovieGenre
                                {
                                    Movie = movie,
                                    Genre = genre
                                };

                                _context.MovieGenres.Add(movieGenre);
                            }

                            _context.Movies.Add(movie);                            
                        }

                        _context.SaveChanges();
                    }
                }
            }
        }

        public void PersistAnswersIfEmpty(AnswerResponse answerResponse)
        {
            if (_context.Answers.Count() == 0)
            {
                // Only permit one thread at a time
                lock (lockObj)
                {
                    // If the database table is still empty, then insert the records
                    if (_context.Answers.Count() == 0)
                    {
                        foreach (var answer in answerResponse.Answer)
                        {
                            _context.Answers.Add(new Answer
                            {
                                QuestionEnum = answer.Key.ToString(),
                                AnswerText = answer.Value
                            });
                        }

                        _context.SaveChanges();
                    }
                }
            }
        }

        private static List<Shared.Objects.Movie> LoadRecords(string dataFileName)
        {
            string rawJson = System.IO.File.ReadAllText(dataFileName);
            return JsonConvert.DeserializeObject<List<Shared.Objects.Movie>>(rawJson);
        }
    }
}
