#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build

WORKDIR /src
COPY ["DockerComposeApp.UnitTests/DockerComposeApp.UnitTests.csproj", "DockerComposeApp.UnitTests/"]
RUN dotnet restore "DockerComposeApp.UnitTests/DockerComposeApp.UnitTests.csproj"
COPY . .
WORKDIR "/src/DockerComposeApp.UnitTests"
RUN dotnet test "DockerComposeApp.UnitTests.csproj"

WORKDIR /src
COPY ["DockerComposeApp.Api/DockerComposeApp.Api.csproj", "DockerComposeApp.Api/"]
RUN dotnet restore "DockerComposeApp.Api/DockerComposeApp.Api.csproj"
COPY . .
WORKDIR "/src/DockerComposeApp.Api"
RUN dotnet build "DockerComposeApp.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "DockerComposeApp.Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DockerComposeApp.Api.dll"]