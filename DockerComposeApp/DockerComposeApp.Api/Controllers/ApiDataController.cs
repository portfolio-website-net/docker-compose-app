﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DockerComposeApp.Api.Services.Interfaces;
using DockerComposeApp.Shared.Requests;
using DockerComposeApp.Shared.Responses;

namespace DockerComposeApp.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ApiDataController : ControllerBase
    {
        private readonly ILogger<ApiDataController> _logger;
        private readonly IDataProcessingService _dataProcessingService;

        public ApiDataController(ILogger<ApiDataController> logger,
            IDataProcessingService dataProcessingService)
        {
            _logger = logger;
            _dataProcessingService = dataProcessingService;
        }

        [HttpGet("HealthCheck")]
        public string HealthCheck()
        {
            return "DockerComposeApp.Api is Healthy";
        }

        [HttpPost("GetAnswers")]
        public AnswerResponse GetAnswers(AnswerRequest answerRequest)
        {
            return _dataProcessingService.GetAnswers(answerRequest);
        }
    }
}
