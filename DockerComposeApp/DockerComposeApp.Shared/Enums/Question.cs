﻿namespace DockerComposeApp.Shared.Enums
{
    public enum Question
    {
        CountSince1975,
        MostCommonYear,
        TotalCastMembers
    }
}
