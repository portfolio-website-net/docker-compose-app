﻿using DockerComposeApp.Shared.Enums;
using System.Collections.Generic;

namespace DockerComposeApp.Shared.Requests
{
    public class AnswerRequest
    {
        public AnswerRequest()
        {
            this.Question = new List<Question>();
        }

        public List<Question> Question { get; set; }
    }
}
