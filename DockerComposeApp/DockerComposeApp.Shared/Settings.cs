namespace DockerComposeApp.Shared
{
    public class Settings
    {
        public string ApiBaseUrl { get; set; }

        public string DataFileName { get; set; }
    }
}
