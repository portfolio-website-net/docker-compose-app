﻿using DockerComposeApp.Shared.Enums;
using System.Collections.Generic;

namespace DockerComposeApp.Shared.Responses
{
    public class AnswerResponse
    {
        public AnswerResponse()
        {
            this.Answer = new Dictionary<Question, string>();
        }

        public Dictionary<Question, string> Answer { get; set; }
    }
}
