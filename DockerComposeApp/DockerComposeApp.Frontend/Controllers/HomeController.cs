﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DockerComposeApp.Frontend.Models;
using Microsoft.Extensions.Options;
using DockerComposeApp.Shared;
using DockerComposeApp.Frontend.Services.Interfaces;
using DockerComposeApp.Shared.Enums;
using DockerComposeApp.Shared.Requests;
using DockerComposeApp.Shared.Responses;

namespace DockerComposeApp.Frontend.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Settings _settings;
        private readonly IDataRequestService _dataRequestService;

        public HomeController(ILogger<HomeController> logger, 
            IOptions<Settings> settings, 
            IDataRequestService dataRequestService)
        {
            _logger = logger;
            _settings = settings.Value;
            _dataRequestService = dataRequestService;
        }

        public IActionResult Index()
        {
            var answerRequest = new AnswerRequest();
            answerRequest.Question.Add(Question.CountSince1975);
            answerRequest.Question.Add(Question.MostCommonYear);
            answerRequest.Question.Add(Question.TotalCastMembers);
            var answerResponse = _dataRequestService.GetAnswers(answerRequest);
            if (answerResponse == null)
            {
                answerResponse = new AnswerResponse();
            }

            return View(answerResponse);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
