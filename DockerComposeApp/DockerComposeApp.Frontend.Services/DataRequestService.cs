﻿using DockerComposeApp.Frontend.Services.Interfaces;
using DockerComposeApp.Shared.Requests;
using DockerComposeApp.Shared.Responses;

namespace DockerComposeApp.Frontend.Services
{
    public class DataRequestService : IDataRequestService
    {
        private readonly INetworkRequestService _networkRequestService;

        public DataRequestService(INetworkRequestService networkRequestService)
        {
            _networkRequestService = networkRequestService;
        }

        public AnswerResponse GetAnswers(AnswerRequest answerRequest)
        {
            return _networkRequestService.SendPostRequest<AnswerResponse>("/ApiData/GetAnswers", answerRequest);
        }
    }
}
