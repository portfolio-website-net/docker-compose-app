﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using DockerComposeApp.Frontend.Services.Interfaces;
using DockerComposeApp.Shared;
using DockerComposeApp.Shared.Requests;
using RestSharp;

namespace DockerComposeApp.Frontend.Services
{
    public class NetworkRequestService : INetworkRequestService
    {
        private readonly Settings _settings;

        public NetworkRequestService(IOptions<Settings> settings)
        {
            _settings = settings.Value;
        }

        public T SendPostRequest<T>(string endpoint, AnswerRequest answerRequest)
        {
            T result = default(T);

            var client = new RestClient(_settings.ApiBaseUrl);
            client.Timeout = 1000 * 60 * 5; // 5 minutes
            var request = new RestRequest(endpoint, DataFormat.Json);
            request.AddJsonBody(answerRequest);
            var response = client.Post(request);

            if (response.IsSuccessful)
            {
                result = JsonConvert.DeserializeObject<T>(response.Content);
            }

            return result;
        }
    }
}
