﻿using DockerComposeApp.Shared.Requests;
using DockerComposeApp.Shared.Responses;

namespace DockerComposeApp.Frontend.Services.Interfaces
{
    public interface IDataRequestService
    {
        AnswerResponse GetAnswers(AnswerRequest answerRequest);
    }
}