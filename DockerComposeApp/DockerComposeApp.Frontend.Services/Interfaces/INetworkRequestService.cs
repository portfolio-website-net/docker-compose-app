﻿using DockerComposeApp.Shared.Requests;

namespace DockerComposeApp.Frontend.Services.Interfaces
{
    public interface INetworkRequestService
    {
        T SendPostRequest<T>(string endpoint, AnswerRequest answerRequest);
    }
}