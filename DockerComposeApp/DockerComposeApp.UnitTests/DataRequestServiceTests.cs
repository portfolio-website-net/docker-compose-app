using Moq;
using DockerComposeApp.Frontend.Services;
using DockerComposeApp.Frontend.Services.Interfaces;
using DockerComposeApp.Shared.Enums;
using DockerComposeApp.Shared.Requests;
using DockerComposeApp.Shared.Responses;
using System.Collections.Generic;
using Xunit;

namespace DockerComposeApp.UnitTests
{
    public class DataRequestServiceTests
    {
        [Fact]
        public void Validate_GetExample_Returns_Valid_Result()
        {
            var mockNetworkRequestService = new Mock<INetworkRequestService>();
            mockNetworkRequestService.Setup(x => x.SendPostRequest<AnswerResponse>(It.IsAny<string>(), It.IsAny<AnswerRequest>()))
                .Returns(new AnswerResponse
                {
                    Answer = new Dictionary<Question, string>() { { Question.CountSince1975, "123"} }
                });

            var dataRequestService = new DataRequestService(mockNetworkRequestService.Object);

            var result = dataRequestService.GetAnswers(new AnswerRequest
            {
                Question = new List<Question>()
                {
                    Question.CountSince1975
                }
            });

            Assert.Equal("123", result.Answer[Question.CountSince1975]);
        }
    }
}
