using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using DockerComposeApp.Api.Services;
using DockerComposeApp.Data.Models;
using DockerComposeApp.Shared;
using DockerComposeApp.Shared.Enums;
using System;
using System.Collections.Generic;
using Xunit;
using Movie = DockerComposeApp.Data.Models.Movie;

namespace DockerComposeApp.UnitTests
{
    public class DataProcessingServiceTests
    {
        [Fact]
        public void Validate_GetExample_Returns_Valid_Result()
        {
            var settings = Options.Create(
                new Settings
                {
                    ApiBaseUrl = "http://test",
                    DataFileName = "movies.json",
                }
            );

            var options = new DbContextOptionsBuilder<Context>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var context = new Context(options);

            context.Movies.Add(new Movie
            {
                MovieId = 1,
                Title = "Test",
                Year = 1975
            });

            context.SaveChanges();

            var dataProcessingService = new DataProcessingService(context, settings);

            var result = dataProcessingService.GetAnswers(new Shared.Requests.AnswerRequest
            {
                Question = new List<Question>() { Question.CountSince1975 }
            });

            Assert.Equal("1", result.Answer[Question.CountSince1975]);
        }
    }
}
