﻿using System.Collections.Generic;

namespace ConsoleApp.Objects
{
    // Converted with https://json2csharp.com
    public class Movie
    {
        public string title { get; set; }
        public int year { get; set; }
        public List<string> cast { get; set; }
        public List<string> genres { get; set; }
    }
}
