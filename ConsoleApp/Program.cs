﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ConsoleApp.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var settings = InitializeSettings();
            var records = LoadRecords(settings.DataFileName);


            var countSince1975 = records.Where(x => x.year >= 1975);
            Console.WriteLine("What is the number of movies since 1975?");
            Console.WriteLine($"{string.Format("{0:n0}", countSince1975.Count())}");
            Console.WriteLine();


            var mostCommonYear = records.GroupBy(x => x.year)
                                        .Select(group => new
                                        {
                                            Year = group.Key,
                                            Count = group.Count()
                                        })
                                        .OrderByDescending(x => x.Count)
                                        .First().Year;
            Console.WriteLine("What is the most common year for movies?");
            Console.WriteLine($"{mostCommonYear}");
            Console.WriteLine();


            var totalCastMembers = records.Sum(x => x.cast.Count());
            Console.WriteLine("What is the total number of all cast members combined (cast members may be counted multiple times)?");
            Console.WriteLine($"{string.Format("{0:n0}", totalCastMembers)}");
            Console.WriteLine();
        }

        private static Settings InitializeSettings()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            var configuration = builder.Build();
            var settings = new Settings();
            ConfigurationBinder.Bind(configuration.GetSection("Settings"), settings);

            return settings;
        }

        private static List<Movie> LoadRecords(string dataFileName)
        {
            string rawJson = System.IO.File.ReadAllText(dataFileName);
            return JsonConvert.DeserializeObject<List<Movie>>(rawJson);
        }
    }
}
